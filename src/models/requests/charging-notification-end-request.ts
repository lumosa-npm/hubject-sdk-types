import { HubjectChargingBaseNotificationRequest } from "./charging-notification-base-request";
import { ProductIDType } from "../types";

export interface HubjectChargingEndNotificationRequest extends HubjectChargingBaseNotificationRequest {

    /**
     * The date and time at which the charging process started.
     */
    ChargingStart?: string;

    /**
     * The date and time at which the charging process stoped.
     */
    ChargingEnd: string;

    /**
     * The date and time at which the session started, e.g. swipe of RFID or cable connected.
     */
    SessionStart?: string;

    /**
     * The date and time at which the session ended. E. g. Swipe of RFID or Cable disconnected.
     */
    SessionEnd?: string;

    /**
     * The difference between MeterValueEnd and MeterValueStart in kWh.
     */
    ConsumedEnergy?: number;
    
    /**
     * The starting meter value in kWh.
     */
    MeterValueStart?: number;    
    
    /**
    * The ending meter value in kWh.
    */
    MeterValueEnd: number;

    /**
    * List of meter values that may have been taken in between (kWh).
    */
    MeterValueInBetween?: number[];

    /**
     * The OperatorID is used to identify the CPO.
     */
    OperatorID?: string;

    /**
     * A pricing product name (for identifying a tariff) that must be unique
     */
    PartnerProductID?: ProductIDType;

    /**
     * 	The date and time at which the penalty time start after the grace period.
     */
    PenaltyTimeStart?: string;
}