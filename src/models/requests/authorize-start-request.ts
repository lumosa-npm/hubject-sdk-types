import { 
  IdentificationType,
  ProductIDType
} from '../types';

export interface HubjectAuthorizeStartRequest {
  /**
   * The Hubject SessionID that identifies the process
   */
  SessionID?: string;

  /**
   * Optional field containing the session id assigned by the CPO to the related operation. 
   * Partner systems can use this field to link their own session handling to HBS processes.
   */
  CPOPartnerSessionID?: string;

  /**
   * Optional field containing the session id assigned by an EMP to the related operation. 
   * Partner systems can use this field to link their own session handling to HBS processes.
   */
  EMPPartnerSessionID?: string;

  /**
   * The OperatorID is defined by Hubject and is used to identify the CPO.
   */
  OperatorID: string;

  /**
   * The ID that identifies the charging spot.
   */
  EvseID?: string;

  /**
   * Authentication data used to authorize the user or car.
   */
  Identification: IdentificationType;

  /**
   * A pricing product name (for identifying a tariff) that must be unique
   */
  PartnerProductID?: ProductIDType;
}
