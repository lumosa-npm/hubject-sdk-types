import { HubjectChargingBaseNotificationRequest } from "./charging-notification-base-request";
import { ProductIDType } from "../types";

export interface HubjectChargingProgressNotificationRequest extends HubjectChargingBaseNotificationRequest {

    /**
     * The date and time at which the charging process started.
     */
    ChargingStart: string;

    /**
     * The date and time at which the charging progress parameters are captured.
     */
    EventOccurred: string;
    
    /**
     * Charging Duration = EventOccurred - Charging Duration. It is a time in millisecond.
     * Either ChargingDuration or ConsumedEnergyProgress should be provided. 
     * Both can also be provided with each progress notification.
     */
    ChargingDuration?: number;

    /**
     * The date and time at which the session started, e.g. swipe of RFID or cable connected.
     */
    SessionStart?: string;

    /**
     * This is consumed energy when from Start of charging process till the charging progress notification generated (EventOccurred)
     * Either ChargingDuration or ConsumedEnergyProgress should be provided. 
     * Both can also be provided with each progress notification.
     */
    ConsumedEnergyProgress?: number;
    
    /**
     * The starting meter value in kWh.
     */
    MeterValueStart?: number;   

    /**
    * List of meter values that may have been taken in between (kWh).
    */
    MeterValueInBetween?: number[];

    /**
     * The OperatorID is used to identify the CPO.
     */
    OperatorID?: string;

    /**
     * A pricing product name (for identifying a tariff) that must be unique
     */
     PartnerProductID?: ProductIDType;
}