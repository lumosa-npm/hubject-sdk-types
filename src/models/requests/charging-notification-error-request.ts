import { HubjectChargingBaseNotificationRequest } from "./charging-notification-base-request";

export interface HubjectChargingErrorNotificationRequest extends HubjectChargingBaseNotificationRequest {
    /**
     * The error code can be chosen from the list
     * 
     * Connector Error: 
     *  - Charging process cannot be started or stopped. EV driver needs to check if the the Plug is properly inserted or taken out from socket.
     * 
     * Critical Error:
     *  - Charging process stopped abruptly. Reason: Physical check at the station is required. Station cannot be reset online.
     *  - Error with the software or hardware of the station locally.
     *  - Communication failure with the vehicle.
     *  - The error needs to be investigated
     *  - Ground Failure
     */
    ErrorType: 
        'Connector Error' |
        'Critical Error';

    /**
     * The CPO can put in the additional information about the error
     */
    ErrorAdditionalInfo?: string;
}