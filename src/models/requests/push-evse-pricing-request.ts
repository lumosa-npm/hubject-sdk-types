import { 
  ActionType
} from "../types";
import { EVSEPricingType } from "../types/evse-pricing-type";

export interface HubjectPushEvsePricingRequest {
  /**
   * Describes the action that has to be performed by Hubject with the provided data.
   */
  ActionType: ActionType;

  /**
   * A list of EVSEs and their respective pricing product relation
   */
  EVSEPricing: EVSEPricingType[];
}
