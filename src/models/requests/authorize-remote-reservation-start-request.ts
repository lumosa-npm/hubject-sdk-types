import { 
  IdentificationType,
  ProductIDType
} from '../types';

export interface HubjectAuthorizeRemoteReservationStartRequest {
  /**
   * The Hubject SessionID that identifies the process
   */
  SessionID?: string;

  /**
   * Optional field containing the session id assigned by the CPO to the related operation. 
   * Partner systems can use this field to link their own session handling to HBS processes.
   */
  CPOPartnerSessionID?: string;

  /**
   * Optional field containing the session id assigned by an EMP to the related operation. 
   * Partner systems can use this field to link their own session handling to HBS processes.
   */
  EMPPartnerSessionID?: string;

  /**
   * The ProviderID is defined by Hubject and is used to identify the EMP. 
   * In case of a positive authorization this field will be filled.
   */
  ProviderID: string;

  /**
   * The ID that identifies the charging spot.
   */
  EvseID: string;

  /**
   * Authentication data used to authorize the user or car.
   */
  Identification: IdentificationType;

  /**
   * A pricing product name (for identifying a tariff) that must be unique
   */
  PartnerProductID?: ProductIDType;

  /**
   * The duration of the reservation in minutes
   */
   Duration?: number;
}
