import { IdentificationType } from "../types";

/**
 * eRoamingChargeDetailRecord is a message that contains charging process details (e.g. meter values).
 */
export interface HubjectChargeDetailRecordRequest {
    /**
     * The Hubject SessionID that identifies the process
     */
    SessionID: string;

    /**
     * Optional field containing the session id assigned by the CPO to the related operation. 
     * Partner systems can use this field to link their own session handling to HBS processes.
     */
    CPOPartnerSessionID?: string;

    /**
     * Optional field containing the session id assigned by an EMP to the related operation. 
     * Partner systems can use this field to link their own session handling to HBS processes.
     */
    EMPPartnerSessionID?: string;

    /**
     * A pricing product name (for identifying a tariff) that must be unique
     */
    PartnerProductID?: string;

    /**
     * The ID that identifies the charging spot.
     */
    EvseID: string;

    Identification: IdentificationType;

    /**
     * The date and time at which the charging process started.
     */
    ChargingStart: string;
    
    /**
     * The date and time at which the charging process stopped.
     */
    ChargingEnd: string;
    
    /**
     * The date and time at which the session started, e.g. swipe of RFID or cable connected.
     */
    SessionStart: string;
    
    /**
     * The date and time at which the session ended. E. g. Swipe of RFID or Cable disconnected.
     */
    SessionEnd: string;
    
    /**
     * The starting meter value in kWh.
     */
    MeterValueStart?: number;

    /**
     * The ending meter value in kWh.
     */
    MeterValueEnd?: number;

    /**
     * List of meter values that may have been taken in between (kWh).
     */
    MeterValueInBetween?: {
        meterValues: number[];
    }

    /**
     * The difference between MeterValueEnd and MeterValueStart in kWh.
     */
    ConsumedEnergy: number;
}