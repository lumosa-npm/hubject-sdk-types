import { IdentificationType } from "../types";

export interface HubjectChargingBaseNotificationRequest {
    /**
     * The Hubject SessionID that identifies the process.
     */
    SessionID: string;

    /**
     * Optional field containing the session ID assigned by the CPO to the related operation.
     * Partner systems can use this field to link their own session handling to HBS processes.
     */
    CPOPartnerSessionID?: string;

    /**
     * Optional field containing the session ID assigned by an EMP to the related operation.
     * Partner systems can use this field to link their own session handling to HBS processes.
     */
    EMPPartnerSessionID?: string;

    /**
     * Authentication data
     */
    Identification: IdentificationType;

    /**
     * The ID that identifies the charging spot.
     */
    EvseID: string;
}