import { 
  ActionType, 
  EvseStatusRecordType
} from "../types";

export interface HubjectPushStatusRequest {
  /**
   * Describes the action that has to be performed by Hubject with the provided data.
   */
  ActionType: ActionType;

  /**
   * A list of Charging Stations status given by the operator
   */
  OperatorEvseStatus: {
    OperatorID: string;
    OperatorName: string;
    EvseStatusRecord: EvseStatusRecordType[];
  }
}
