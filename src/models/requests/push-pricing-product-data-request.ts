import { 
  ActionType, 
  PricingProductDataType 
} from "../types";

export interface HubjectPushPricingProductDataRequest {
  /**
   * Describes the action that has to be performed by Hubject with the provided data.
   */
  ActionType: ActionType;

  /**
   * Details of pricing products offered by a particular operator for a specific provider
   */
  PricingProductData: PricingProductDataType;
}
