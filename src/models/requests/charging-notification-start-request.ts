import { HubjectChargingBaseNotificationRequest } from "./charging-notification-base-request";
import { ProductIDType } from "../types";

export interface HubjectChargingStartNotificationRequest extends HubjectChargingBaseNotificationRequest {
    /**
     * The date and time at which the charging process started.
     */
    ChargingStart: string;

    /**
     * The date and time at which the session started, e.g. swipe of RFID or cable connected.
     */
    SessionStart?: string;
    
    /**
     * The starting meter value in kWh.
     */
    MeterValueStart: number;

    /**
     * The OperatorID is used to identify the CPO.
     */
    OperatorID?: string;

    /**
     * A pricing product name (for identifying a tariff) that must be unique
     */
    PartnerProductID?: ProductIDType;
}