import { 
  EvseDataRecordType, 
  ActionType 
} from "../types";

export interface HubjectPushDataRequest {
  /**
   * Describes the action that has to be performed by Hubject with the provided data.
   */
  ActionType: ActionType;

  OperatorEvseData: {
    /**
     * The provider whose data records are listed below
     */
    OperatorID: string;

    /**
     * Free text for operator
     */
    OperatorName: string;

    /**
     * Charger entries to be registered
     */
    EvseDataRecord: EvseDataRecordType[];
  };
}
