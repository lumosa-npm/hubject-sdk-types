/**
 * START FEE 
 * Can be used in case a fixed fee is charged for the initiation of the charging session. 
 * This is a fee charged on top of the main base price defined in the field "PricePerReferenceUnit" for any particular pricing product.
 * 
 * FIXED FEE
 * Can be used if a single price is charged irrespective of charging duration or energy consumption (for instance if all sessions are to be charged a single fixed fee). 
 * When used, the value set in the field "PricePerReferenceUnit" for the main base price of respective pricing product SHOULD be set to zero.
 * 
 * PARKING FEE Can be used in case sessions are to be charged for both parking and charging. 
 * When used, it needs to be specified in the corresponding service offer on the HBS Portal when parking applies (e.g. from session start to charging start and charging end to session end or for the entire session duration, or x-minutes after charging end, etc)
 * 
 * MINIMUM FEE
 * Can be used in case there is a minimum fee to be paid for all charging sessions. 
 * When used, this implies that the eventual price to be paid cannot be less than this minimum fee but can however be a price above/greater than the minimum fee.
 * 
 * MAXIMUM FEE
 * Can be used in case there is a maximum fee to be charged for all charging sessions. 
 * When used, this implies that the eventual price to be paid cannot be more than this maximum fee but can however be a price below/lower than the maximum fee.
 */
export declare type AdditionalReferenceType =
    'START FEE' |
    'FIXED FEE' |
    'PARKING FEE' |
    'MINIMUM FEE' |
    'MAXIMUM FEE'