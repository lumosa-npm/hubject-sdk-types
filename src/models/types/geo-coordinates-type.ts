export interface GeoCoordinatesType {
    /**
     * Geocoordinates using Google Structure
     */
    Google?: {
        /**
         * A string that MUST be valid with respect to the following regular expression:
         * ^-?1?\d{1,2}\.\d{1,6}\s*\,?\s*-?1?\d{1,2}\.\d{1,6}$
         * 
         * The expression validates the string as geo coordinates with respect to the Google standard. 
         * The string contains latitude and longitude (in this sequence) separated by a space.
         */
        Coordinatea: string;
    }

    /**
     * Geocoordinates using DecimalDegree Structure
     */
    DecimalDegree?: {
        /**
         * A string that MUST be valid with respect to the following regular expression:
         * ^-?1?\d{1,2}\.\d{1,6}$
         * 
         * The expression validates the string as a geo coordinate (longitude or latitude) with decimal degree syntax.
         * Examples: 
         *   “9.360922”, “-21.568201”
         */
        Longitude: string;

        /**
         * A string that MUST be valid with respect to the following regular expression:
         * ^-?1?\d{1,2}\.\d{1,6}$
         * 
         * The expression validates the string as a geo coordinate (longitude or latitude) with decimal degree syntax.
         * Examples: 
         *   “9.360922”, “-21.568201”
         */
        Latitude: string;
    }

    /**
     * Geocoordinates using DegreeMinutesSeconds Structure
     */
    DegreeMinuteSeconds?: {
        /**
         * A string that MUST be valid with respect to the following regular expression:
         * ^-?1?\d{1,2}°[ ]?\d{1,2}'[ ]?\d{1,2}\.\d+''$
         * 
         * The expression validates the string as a geo coordinate (longitude or latitude) consisting of degree, minutes, and seconds
         * Examples: 
         *   “9°21'39.32''”, “-21°34'23.16''
         */
        Longitude: string;

        /**
         * A string that MUST be valid with respect to the following regular expression:
         * ^-?1?\d{1,2}°[ ]?\d{1,2}'[ ]?\d{1,2}\.\d+''$
         * 
         * The expression validates the string as a geo coordinate (longitude or latitude) consisting of degree, minutes, and seconds
         * Examples: 
         *   “9°21'39.32''”, “-21°34'23.16''
         */
        Latitude: string;
    }
}