export interface InfoTextType {
    /**
     * The language in which the additional info text is provided
     */
    lang: string;

    /**
     * The Additional Info text
     */
    value: string;
}