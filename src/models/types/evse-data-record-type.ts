import { AddressIso19773Type } from "./address-type";
import { AuthenticationModeType } from "./authentication-mode-type";
import { ChargingFacilityType } from "./charging-facility-type";
import { GeoCoordinatesType } from "./geo-coordinates-type";
import { InfoTextType } from "./info-text-type";
import { PaymentOptionType } from "./payment-option-type";
import { PeriodType } from "./period-type";
import { PlugType } from "./plug-type";
import { ValueAddedServiceType } from "./value-added-service-type";

export interface EvseDataRecordType {
    /**
     * In case that the operation “PullEvseData” is performed with the parameter “LastCall”, 
     * Hubject assigns this attribute to every response EVSE record in order to return the changes compared to the last call.
     */
    deltaType?: 'udpate' | 'insert' | 'delete';

    /**
     * The attribute indicates the date and time of the last update of the record. 
     * Hubject assigns this attribute to every response EVSE record.
     */
    lastUpdate?: string;

    /**
     * The ID that identifies the charging spot.
     */
    EvseID: string;

    /**
     * The ID that identifies the charging station.
     */
    ChargingPoolID?: string;

    /**
     * Name of the charging stations
     */
    ChargingStationNames: InfoTextType[];

    /**
     * Name of the charging point manufacturer
     */
    HardwareManufacturer?: string;

    /**
     * URL that redirect to an online image of the related EVSEID
     */
    ChargingStationImage?: string;

    /**
     * Name of the Sub Operator owning the Charging Station
     */
    SubOperatorName?: string;

    /**
     * Address of the charging station.
     */
    Address: AddressIso19773Type;

    /**
     * Geolocation of the charging station.
     */
    GeoCoordinates: GeoCoordinatesType;

    /**
     * 	List of plugs that are supported.
     */
    Plugs: PlugType[];

    /**
     * Informs is able to deliver different power outputs.
     */
    DynamicPowerLevel?: boolean;

    /**
     * List of facilities that are supported.
     */
    ChargingFacilities: ChargingFacilityType[];

    /**
     * If the Charging Station provides only renewable energy then the value MUST be” true”, 
     * if it use grey energy then value MUST be “false”.
     */
    RenewableEnergy: boolean;

    /**
     * List of energy source that the charging station uses to supply electric energy.
     */
    EnergySource?: unknown[];

    /**
     * Environmental Impact produced by the energy sources used by the charging point
     */
    EnvironmentalImpact?: {
        /**
         * Total CO2 emited by the energy source being used by this charging station to supply energy to EV. Units are in g/kWh
         */
        CO2Emission: number;

        /**
         * Total NuclearWaste emited by the energy source being used by this charging station to supply energy to EV. Units are in g/kWh
         */
        NuclearWaste: number;
    }

    /**
     * This field gives the information how the charging station provides metering law data.
     */
    CalibrationLawDataAvailability:
        'Local' |
        'External' |
        'Not Available';

    /**
     * List of authentication modes that are supported.
     */
    AuthenticationModes: AuthenticationModeType[];

    /**
     * This field is used if the EVSE has a limited capacity (e.g. built-in battery). 
     * Values must be given in kWh.
     */
    MaxCapacity?: number;

    /**
     * List of payment options that are supported.
     */
    PaymentOptions: PaymentOptionType[];

    /**
     * 	List of value added services that are supported.
     */
    ValueAddedServices: ValueAddedServiceType[];

    /**
     * Specifies how the charging station can be accessed.
     * 
     * Free publicly accessible	    EV Driver can reach the charging point without paying a fee, e.g. street, free public place, free parking lot, etc.
     * Restricted access	        EV Driver needs permission to reach the charging point, e.g. Campus, building complex, etc.
     * Paying publicly accessible	EV Driver needs to pay a fee in order to reach the charging point, e.g. payable parking garage, etc.
     * Test Station	                Station is just for testing purposes. Access may be restricted.
     */
    Accessibility: 
        'Free publicly accessible' | 
        'Restricted access' | 
        'Paying publicly accessible' | 
        'Test Station';

    /**
     * Inform the EV driver where the ChargingPoint could be accessed.
     * 
     * OnStreet	                The charging station is located on the street
     * ParkingLot	            The Charging Point is located inside a Parking Lot
     * ParkingGarage	        The Charging Point is located inside a Parking Garage
     * UndergroundParkingGarage	The Charging Point is located inside an Underground Parking Garage
     */
    AccessibilityLocation?: 
        'OnStreet' | 
        'ParkingLot' | 
        'ParkingGarage' | 
        'UndergroundParkingGarage';

    /**
     * Phone number of a hotline of the charging station operator.
     * regex: ^\+[0-9]{5,15}$
     */
    HotlinePhoneNumber: string;

    /**
     * Optional information.
     */
    AdditionalInfo?: InfoTextType;

    /**
     * Last meters information regarding the location of the Charging Station
     */
    ChargingStationLocationReference?: InfoTextType;

    /**
     * In case that the charging spot is part of a bigger facility (e.g. parking place), 
     * this attribute specifies the facilities entrance coordinates.
     */
    GeoChargingPointEntrance?: GeoCoordinatesType;

    /**
     * Set in case the charging spot is open 24 hours.
     */
    IsOpen24Hours: boolean;

    /**
     * Opening time in case that the charging station cannot be accessed around the clock.
     */
    OpeningTimes?: {
        /**
         * The starting and end time for pricing product applicability in the specified period
         */
        Period: PeriodType;

        /**
         * Day values to be used in specifying periods on which the product is available. 
         * Workdays = Monday – Friday, 
         * Weekend = Saturday – Sunday
         */
        On: 
            'Everyday' | 
            'Workdays' | 
            'Weekend' | 
            'Monday' | 
            'Tuesday' | 
            'Wednesday' | 
            'Thursday' | 
            'Friday' | 
            'Saturday' | 
            'Sunday';
    }[];

    /**
     * Hub operator
     */
    HubOperatorID?: string;

    /**
     * Identification of the corresponding clearing house in the event that roaming between different clearing houses MUST be processed in the future.
     */
    ClearinghouseID?: string;

    /**
     * Is eRoaming via intercharge at this charging station possible? 
     * If set to "false" the charge spot will not be started/stopped remotely via Hubject.
     */
    IsHubjectCompatible: boolean;

    /**
     * Values: true / false / auto 
     * 
     * This attribute indicates whether a CPO provides (dynamic) EVSE Status info in addition to the (static) EVSE Data for this EVSERecord. 
     * Value auto is set to true by Hubject if the operator offers Hubject EVSEStatus data.
     */
    DynamicInfoAvailable: 
        'true' | 
        'false' | 
        'auto';
}