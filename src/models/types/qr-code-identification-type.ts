export interface QRCodeIdentificationType {
    /**
     * Contract identifier Hubject will automatically convert all characters from lower case to upper case
     */
    EvcoID: string;

    /**
     * The hashed pin information. 
     * This field can be provided only when uploading Authentication data. In Authorization requests this field must be null!
     */
    HashedPIN?: {
        /**
         * Hash value created by partner
         */
        Value: string;

        /**
         * Function that was used to generate the hash value.
         */
        Function: {
            /**
             * Hash value is based on Bcrypt.
             */
            Bcrypt: string;
        }

        /**
         * Field for hashing data related to OICP v2.1. It is unused in later versions.
         */
        LegacyHashData?: {
            /**
             * Function used for hashing of the PIN at the partner.
             */
            Function: 'MD5' | 'SHA-1'

            /**
             * 	The salt value used by the partner for hashing the PIN.
             */
            Salt?: string;

            /**
             * PIN hash at the partner.
             */
            Value?: string;
        }
    }

    /**
     * The pin number, this field is required in Authorization requests!
     */
    PIN?: string;
}