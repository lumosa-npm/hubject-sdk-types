
/**
* EUR   Euro
* CHF   Swiss franc
* CAD   Canadian Dollar
* GBP   Pound sterling
*/
export declare type CurrencyIdType =
    'EUR' |
    'CHF' |
    'CAD' |
    'GBP';
