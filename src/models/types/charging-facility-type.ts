import { ChargingModeType } from "./charging-mode-type";

export interface ChargingFacilityType {
    /**
     * Charging Facility power type (e.g. AC or DC)
     */
    PowerType: 
        'AC_1_PHASE' |
        'AC_3_PHASE' |
        'DC';

    /**
     * Voltage (Line to Neutral) of the Charging Facility
     */
    Voltage?: number;

    /**
     * Amperage of the Charging Facility
     */
    Amperage?: number;

    /**
     * Charging Facility power in kW
     */
    Power: number;

    /**
     * List of charging modes that are supported.
     */
    ChargingModes?: ChargingModeType[];
}