/**
 * Information specifying whether the user is authorized to charge or not.
 */
 export declare type AuthorizationStatusType = 
    'Authorized' | 
    'NotAuthorized';