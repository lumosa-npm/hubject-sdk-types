/**
 * The status of the charging spot.
 */
 export declare type EvseStatusType = 
    'Available' | 
    'Reserved' | 
    'Occupied' | 
    'OutOfService' | 
    'EvseNotFound' | 
    'Unknown';