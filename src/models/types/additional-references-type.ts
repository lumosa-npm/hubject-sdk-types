import { AdditionalReferenceType } from "./additional-reference-type";
import { ReferenceUnitType } from "./reference-unit-type";

export interface AdditionalReferencesType {
    /**
     * Additional pricing components to be considered in addition to the base pricing
     */
    AdditionalReference: AdditionalReferenceType;

    /**
     * Additional reference units that can be used in defining pricing products
     */
    AdditionalReferenceUnit: ReferenceUnitType;

    /**
     * A price in the given currency 
     */
    PricePerAdditionalReferenceUnit: number;
}