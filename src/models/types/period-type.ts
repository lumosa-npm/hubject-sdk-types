export interface PeriodType {
    /**
     * The opening time.
     * regex: [0-9]{2}:[0-9]{2}
     */
    begin: string;

    /**
     * The closing time.
     * regex: [0-9]{2}:[0-9]{2}
     */
    end: string;
}