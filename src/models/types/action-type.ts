/**
 * Describes the action that has to be performed by Hubject with the provided data.
 */
export declare type ActionType = 
    'fullLoad' | 
    'update' | 
    'insert' | 
    'delete';
