/**
 * The type of ChargingNotification: Start, Progress, End, Error
 */
 export declare type ChargingNotificationType = 
    'Start' | 
    'Progress' | 
    'End' | 
    'Error';

