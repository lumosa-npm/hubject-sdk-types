export interface AddressIso19773Type {
    /**
     * The CountryCodeType allows for Alpha-3 country codes only as of OICP 2.2 and OICP 2.3
     * For Alpha-3 (three-letter) country codes as defined in ISO 3166-1.
     */
    Country: string;

    City: string;

    Street: string;

    PostalCode: string;

    HouseNum: string;

    Floor?: string;

    Region?: string;

    ParkingFacility?: boolean;

    ParkingSpot?: string;

    /**
     * The expression validates a string as a Time zone with UTC offset.
     * regex: [U][T][C][+,-][0-9][0-9][:][0-9][0-9]
     * 
     * Examples:
     *   UTC+01:00
     *   UTC-05:00
     */
    TimeZone?: string;
}