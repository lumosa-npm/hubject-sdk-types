/**
 * The Type of the used RFID card like mifareclassic, desfire.
 */
 export declare type RFIDType = 
    'mifareCls' | 
    'mifareDes' | 
    'calypso' | 
    'nfc' | 
    'mifareFamily';
