import { ProductIDType } from ".";

export interface EVSEPricingType {
    /**
     * The EvseID of an EVSE for which the defined pricing products are applicable
     * regex: ^(([A-Za-z]{2}\*?[A-Za-z0-9]{3}\*?E[A-Za-z0-9\*]{1,30})|(\+?[0-9]{1,3}\*[0-9]{3}\*[0-9\*]{1,32}))$
     */
    EvseID: string;

    /**
     * The EMP for whom the pricing data is applicable. In case the data is to be made available for all EMPs (e.g. for Offer-to-All prices), 
     * the asterix character (*) can be set as the value in this field.
     * regex: ^([A-Za-z]{2}\-?[A-Za-z0-9]{3}|[A-Za-z]{2}[\*|-]?[A-Za-z0-9]{3})$
     */
     ProductID: string | '*';

    /**
     * A list of pricing products applicable per EvseID
     */
    EvseIDProductList: ProductIDType[];
}