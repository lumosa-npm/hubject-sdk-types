import { RFIDType } from "./rfid-type";

/**
 * Authentication data details. 
 * The data structure differs depending on the authentication technology
 */
export interface RFIDIdentificationType {
  /**
   * The UID from the RFID-Card. 
   * It SHOULD be read´from left to right using big-endian format. 
   * Hubject will automatically convert all characters from lower case to upper case
   */
  UID: string;

  /**
   * Contract identifier
   */
  EvcoID?: string;

  /**
   * The Type of the used RFID card like mifareclassic, desfire
   */
  RFID: RFIDType;

  /**
   * A number printed on a customer’s card for manual authorization (e.q. via a call center)
   */
  PrintedNumber?: string;

  /**
   * Until when this card is valid. Should not be set if card does not have an expiration
   */
  ExpiryDate?: string;
}