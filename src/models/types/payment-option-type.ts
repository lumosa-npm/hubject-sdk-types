/**
 * No Payment   Free.
 * Direct       e. g. Cash, Card, SMS, …
 * Contract     i. e. Subscription.
 */
export declare type PaymentOptionType =
    'No Payment' |
    'Direct' |
    'Contract';