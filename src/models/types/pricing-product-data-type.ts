import { CurrencyIdType } from "./currency-id-type";
import { PricingProductDataRecordType } from "./pricing-product-data-record-type";
import { ReferenceUnitType } from "./reference-unit-type";

export interface PricingProductDataType {
    /**
     * The provider whose data records are listed below.
     */
    OperatorID: string;

    /**
     * Free text for operator
     */
    OperatorName?: string;

    /**
     * The EMP for whom the pricing data is applicable. 
     * In case the data is to be made available for all EMPs (e.g. for Offer-to-All prices), the asterix character (*) can be set as the value in this field.
     */
    ProviderID: string;

    /**
     * A default price for pricing sessions at undefined EVSEs
     */
    PricingDefaultPrice: number;

    /**
     * Currency for default prices
     */
    PricingDefaultPriceCurrency: CurrencyIdType;

    /**
     * Default Reference Unit in time or kWh
     */
    PricingDefaultReferenceUnit: ReferenceUnitType;

    /**
     * A list of pricing products
     */
    PricingProductDataRecords: PricingProductDataRecordType[];
}