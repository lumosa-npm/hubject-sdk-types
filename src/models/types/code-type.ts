
/**
* 	000	Success.
*	001	Hubject system error.
*	002	Hubject database error.
*	009	Data transaction error.
*	017	Unauthorized Access.
*	018	Inconsistent EvseID.
*	019	Inconsistent EvcoID.
*	021	System error.
*	022	Data error.
*	101	QR Code Authentication failed – Invalid Credentials.
*	102	RFID Authentication failed – invalid UID.
*	103	RFID Authentication failed – card not readable.
*	105	PLC Authentication failed - invalid EvcoID.
*	106	No positive authentication response.
*	110	QR Code App Authentication failed – time out error.
*	120	PLC (ISO/ IEC 15118) Authentication failed – invalid underlying EvcoID.
*	121	PLC (ISO/ IEC 15118) Authentication failed – invalid certificate.
*	122	PLC (ISO/ IEC 15118) Authentication failed – time out error.
*	200	EvcoID locked.
*	210	No valid contract.
*	300	Partner not found.
*	310	Partner did not respond.
*	320	Service not available.
*	400	Session is invalid.
*	501	Communication to EVSE failed.
*	510	No EV connected to EVSE.
*	601	EVSE already reserved.
*	602	EVSE already in use/ wrong token.
*	603	Unknown EVSE ID.
*	604	EVSE ID is not Hubject compatible.
*	700	EVSE out of service.
*/
export declare type CodeType =
    '000' |
    '001' |
    '002' |
    '009' |
    '017' |
    '018' |
    '019' |
    '021' |
    '022' |
    '101' |
    '102' |
    '103' |
    '105' |
    '106' |
    '110' |
    '120' |
    '121' |
    '122' |
    '200' |
    '210' |
    '300' |
    '310' |
    '320' |
    '400' |
    '501' |
    '510' |
    '601' |
    '602' |
    '603' |
    '604' |
    '700';