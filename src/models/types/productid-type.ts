/**
 * The ProductIDType defines some standard values.
 * The type however also supports custom ProductIDs that can be specified by partners (as a string of 50 characters maximum length).
 */
 export declare type ProductIDType = 
    'Standard Price' | 
    'AC1' | 
    'AC3' | 
    'DC' | 
    'CustomProductID';