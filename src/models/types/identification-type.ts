import { QRCodeIdentificationType } from "./qr-code-identification-type";
import { RFIDIdentificationType } from "./rfid-identification-type";

/**
 * Authentication data used to authorize the user or car.
 */
export interface IdentificationType {
    /**
     * 	Authentication data details for eAuthorize start and stop using mifare rfid cards
     */
     RFIDMifareFamilyIdentification?: {
        /**
         * The UID from the RFID-Card.
         * It SHOULD be read from left to right using big-endian format.
         * Hubject will automatically convert all characters from lower case to upper case
         */
        UID: string;
    }
    
    /**
     * 
     */
    RFIDIdentification?: RFIDIdentificationType;

    /**
     * Authentication data details using QR code
     */
    QRCodeIdentification?: QRCodeIdentificationType;

    /**
     * Authentication required for Plug&Charge (EMAID/EVCOID)
     */
    PlugAndChargeIdentification?: {
        /**
         * Contract identifier
         */
        EvcoID: string;
    }

    /**
     * Authentication data details for eAuthorize remote start and stop
     */
    RemoteIdentification?: {
        /**
         * Contract identifier Hubject will automatically convert all characters from lower case to upper case
         */
        EvcoID: string;
    }
}