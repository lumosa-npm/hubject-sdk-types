import { PeriodType } from "./period-type";

/**
 * A list indicating when the pricing product is applicable
 */
export interface ProductAvailabilityTimesType {
    /**
     * The starting and end time for pricing product applicability in the specified period
     */
    Period: PeriodType;

    /**
     * Day values to be used in specifying periods on which the product is available
     */
    On: 
        'Everyday' |
        'Workdays' |
        'Weekend' |
        'Monday' |
        'Tuesday' |
        'Wednesday' |
        'Thursday' |
        'Friday' |
        'Saturday' |
        'Sunday';
}