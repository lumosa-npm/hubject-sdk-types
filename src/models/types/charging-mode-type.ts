/**
*	Mode_1	conductive connection between a standard socket-outlet of an AC supply network and electric vehicle without communication or additional safety features (IEC 61851-1)
*	Mode_2	conductive connection between a standard socket-outlet of an AC supply network and electric vehicle with communication and additional safety features (IEC 61851-1)
*	Mode_3	conductive connection of an EV to an AC EV supply equipment permanently connected to an AC supply network with communication and additional safety features (IEC 61851-1)
*	Mode_4	conductive connection of an EV to an AC or DC supply network utilizing a DC EV supply equipment, with (high-level) communication and additional safety features (IEC 61851-1)
*	CHAdeMO	CHAdeMo Specification
*/
export declare type ChargingModeType = 
    'Mode_1' |
    'Mode_2' |
    'Mode_3' |
    'Mode_4' |
    'CHAdeMO';