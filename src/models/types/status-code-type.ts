import { CodeType } from "./code-type";

/**
 * The structure consists of a defined code, an optional functional description of the status, and optional additional information. 
 * It can be used e.g. to send error details or detailed reasons for a certain process or system behavior. 
 * The optional AdditionalInfo field can be used in order to provide further individual (non-standardized) information.
 */
export interface StatusCodeType {
    /**
     * To be selected from valid range
     */
    Code: CodeType;

    /**
     * Description
     */
    Description?: string;

    /**
     * More information can be provided here
     */
    AdditionalInfo?: string;
}