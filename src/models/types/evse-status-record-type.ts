import { EvseStatusType } from "./evse-status-type";

export interface EvseStatusRecordType {
    EvseID: string;
    EvseStatus: EvseStatusType;
}