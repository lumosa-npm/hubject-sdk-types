import { CurrencyIdType, ProductIDType } from ".";
import { AdditionalReferencesType } from "./additional-references-type";
import { ProductAvailabilityTimesType } from "./product-availability-times-type";
import { ReferenceUnitType } from "./reference-unit-type";

export interface PricingProductDataRecordType {
    /**
     * A pricing product name (for identifying a tariff) that MUST be unique
     */
    ProductID: ProductIDType;

    /**
     * Reference unit in time or kWh
     */
    ReferenceUnit: ReferenceUnitType;

    /**
     * Currency for default prices
     */
    ProductPriceCurrency: CurrencyIdType;

    /**
     * A price per reference unit
     */
    PricePerReferenceUnit: number;

    /**
     * A value in kWh
     */
    MaximumProductChargingPower: number;

    /**
     * Set to TRUE if the respective pricing product is applicable 24 hours a day. 
     * If FALSE, the respective applicability times SHOULD be provided in the field “ProductAvailabilityTimes”.
     */
    IsValid24hours: boolean;

    /**
     * A list indicating when the pricing product is applicable
     */
    ProductAvailabilityTimes: ProductAvailabilityTimesType[];

    /**
     * A list of additional reference units and their respective prices
     */
    AdditionalReferences?: AdditionalReferencesType[];
}