
/**
* HOUR
* KILOWATT_HOUR
* MINUTE
*/
export declare type ReferenceUnitType =
    'HOUR' |
    'KILOWATT_HOUR' |
    'MINUTE';
