import { AuthorizationStatusType } from "../types/authorization-status-type";
import { IdentificationType } from "../types/identification-type";

export interface HubjectAuthorizeStopResponse {
  /**
   * The Hubject SessionID that identifies the process
   */
  SessionID?: string;
  
  /**
   * Optional field containing the session id assigned by the CPO to the related operation. 
   * Partner systems can use this field to link their own session handling to HBS processes.
   */
  CPOPartnerSessionID?: string;
  
  /**
   * Optional field containing the session id assigned by an EMP to the related operation. 
   * Partner systems can use this field to link their own session handling to HBS processes.
   */
  EMPPartnerSessionID?: string;
  
  /**
   * The ProviderID is defined by Hubject and is used to identify the EMP. In case of a positive authorization this field will be filled.
   */
  ProviderID?: string;
  
  /**
   * Information specifying whether the user is authorized to charge or not.
   */
  AuthorizationStatus: AuthorizationStatusType;
  
  /**
   * Structured status details. Can be used to specify the reason for a failed authorization
   */
  Identification: IdentificationType;
}
