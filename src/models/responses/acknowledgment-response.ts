import { StatusCodeType } from "../types";

/**
 * The acknowledgement is a message that is sent in response to several requests.
 */
export interface HubjectAcknowledgmentResponse {
  /**
   * If result is true, the message was received and the respective operation was performed successfully.
   * If result is false, the message was received and the respective operation was not performed successfully.
   */
  Result: boolean;

  /**
   * Structured status details.
   * This can be used e.g. for failure messages or further information regarding the result.
   */
  StatusCode: StatusCodeType;

  /**
   * Represents the service process. 
   * In some cases the current SessionID is returned to the service requestor in this field.
   */
  SessionID?: string;

  /**
   * Optional field containing the session id assigned by the CPO to the related operation.
   */
  CPOPartnerSessionID?: string;

  /**
   * Optional field containing the session id assigned by an EMP to the related operation.
   */
  EMPPartnerSessionID?: string;
}
