# hubject-sdk-types

Hubject sdk type definitions

## Usage

This package is used in [@lumosa-npm/hubject-sdk](https://gitlab.com/lumosa-npm/hubject-sdk). Nevertheless, it can be used as standalone package for clients, e.g. browser apps that on only need to ensure strong typing.